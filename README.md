Code is written in Python 3.6.9 and requires Python 3+ 

This repository contains all the code required to run malaria evolution experiments. 

Experiments begin with equilbrium populations, which are stored as pickles. The host population and the vector population are pickled as separate files. Parasites are contained within the host and vector populations. Sample equilibrium populations are included here for the 1200 vector, 30 strain condition.

New populations must undergo burnin process after founding to reach equilibrium. This is a 3 stage process, running scripts in this order:

1. burnin_population.py
1. evolve_from_burnin.py
1. population_evolution.py

In addition to the standard libraries imported in the code, all scripts require:

	config.py
	genome_within_host.py
	vector.py

* config.py contains all parameters
* genome_within_host.py contains the classes Host_Population, Host, and Genome
* vector.py contains the classes Vector_Population and Vector

Explanation of output text files:

* epi_data: Contains population measures of endemicity, infection and exposure rates in both host and vector

* genome_data: Contains full genome of every gametocyte in the population, in addition to host information. 

* pop_data: Contains information for each host, including parasite  at all life stages, treatment status, and presence of resistance

* strain_data: Contains strain-specific infection information for each host, including level of strain-specific immunity and strain abundance

* vector_data: Contains information about the vector population, including age and infection status


# **1. Found a new population: burnin_population.py**

If infection begins in a totally naive population, the infection cycle will be synchronized, resulting in oscillations. Therefore, the first step is to create a population with a baseline level of immunity,

This script creates a population of hosts and runs for 250 timesteps. Every 5 timesteps, a new host is infected. There is no mortality and there are no vectors. By the conclusion, the population have a broad mix of infection stages and levels of immunity.

Input:

* None
	
Output: 

* Burnin_Host_PID.pickle, where PID is the unique OS process ID.
* genome_dataPID.txt
* vector_dataPID.txt
* strain_dataPID.txt
* pop_dataPID.txt
* epi_dataPID.txt


**Values in the config to set:**

Ensure there is no mortality:

* parasitemia_mort = 100 
* min_RBC_number = 0
* age_mort = False
* background_mort_rate = 0

Infection is pre-determined, not vector-based:

* vector_pop_size = 0

To ensure uniformity of population mutation supply when treatment is introduced:

* mutation_rate = 0

# **2. Start normal evolution: evolve_from_burnin.py**

This script gives hosts a uniform distribution of ages, assigns hyperparasitemia mortalities, and creates a vector population with an infectious vector so that one host will be infected during the first time step. Note: The first approximately 300 timesteps of this stage are memory-intensive.

Input:

* Burnin_Host_PID.pickle

Output:

* Hostpop_Time_t_PID.pickle, where t is the timestep and PID is the OS process ID
* Vectpop_Time_t_PID.pickle
* genome_dataPID.txt
* vector_dataPID.txt
* strain_dataPID.txt
* pop_dataPID.txt
* epi_dataPID.txt

**Values in the config to set:**

* mutation_rate = 0


# **3. Evolve populations to equilibrium: population_evolution.py**


Equilibrium is determined by measures of endemicity and EIR. Using the conditions in this manuscript, that takes 2000 timesteps (not including the 250 timestep burnin).

Input:

* Hostpop_Time_t_PID.pickle, where t is the timestep and PID is the OS process ID
* Vectpop_Time_t_PID.pickle

Output:

* Hostpop_Time_t_PID.pickle, where t is the timestep and PID is the OS process ID
* Vectpop_Time_t_PID.pickle
* genome_dataPID.txt
* vector_dataPID.txt
* strain_dataPID.txt
* pop_dataPID.txt
* epi_dataPID.txt

**Values in the config to set:**

* mutation_rate = 0

# **4. Continue evolution: population_evolution.py**

After equilibrium, all subsequent evolution is done as in step 3, with parameters set as needed.



File write frequency is controlled by the config value data_write_freq

Because of the size of the genome_data file, it can be less frequently by using a larger value for genome_data_write_freq