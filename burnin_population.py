import os
import genome_within_host
from config import *
import vector
import random
import pickle
import time

start = time.time() #Allows setting duration of run time in seconds
pid = os.getpid() #PID used as unique identifier on output files
hostpop = genome_within_host.Host_Population.found_population()
vectpop = vector.Vector_Population.found_vect_pop() #Empty vector population created because the host daily cycle accesses the vector population
all_files = genome_within_host.Host_Population.open_files(pid)

now = time.time()

#d = timestep
d = 0

while now-start < 14400 and d <250:
    d+=1
    if d%5==0: #Every 5 timesteps, infect a random host with merozoites
        host = random.choice(hostpop.hosts)
        for i in range(host_inoculation):
            host.mers.append(genome_within_host.Genotype())
                             
    hostpop.host_pop_cycle(d)
    vectpop.vector_cycle(d, hostpop)  
    hostpop.write_data(all_files, vectpop, d)
    now = time.time()
            

with open("Burnin_Host_" + str(pid) + ".pickle", 'wb') as f:
    pickle.dump(hostpop, f, pickle.HIGHEST_PROTOCOL)
f.close()

genome_within_host.Host_Population.close_files(all_files)    

