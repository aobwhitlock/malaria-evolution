import os
import genome_within_host
from config import *
import vector
import glob
import numpy.random as rnd
import time

start = time.time()
pid = os.getpid()

burninh = glob.glob("Burnin_H*") #Selects host population pickle in the file format Burnin_H
hostpop = genome_within_host.Host_Population.unpickle_host_pop(burninh[0])

for h in hostpop.hosts: #Set host age and parasitemia mortality thresholds
    h.parasitemia_mort = rnd.poisson(parasitemia_mort*10)/1000
    h.age = rnd.randint(host_lifespan)   
    
vectpop = vector.Vector_Population.found_vect_pop()
vectpop.vectors[0].sporozoites.append(genome_within_host.Genotype()) #Infect one vector of age 0, which will feed at timestep 0, immediately infecting a host
vectpop.vectors[0].age = 0

all_files = genome_within_host.Host_Population.open_files(pid)

now = time.time()
d = 250 
while d < 2251 and now-start < 3420:
    d += 1
    hostpop.host_pop_cycle(d) 
    vectpop.vector_cycle(d, hostpop)
    print(d)
    if d%data_write_freq == 0: 
        hostpop.write_data(all_files, vectpop, d)
    if d%pickle_freq == 0 and d > 0 and d != duration:
        hostpop.pickle_host_pop(d, pid)
        vectpop.pickle_vect_pop(d, pid)
    if d%100 == 0:
        for eafile in all_files:
            all_files[eafile].flush()
    now = time.time()
genome_within_host.Host_Population.close_files(all_files)
hostpop.pickle_host_pop(d, pid)
vectpop.pickle_vect_pop(d, pid)