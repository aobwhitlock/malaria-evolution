import numpy as np
import random
import numpy.random as rnd
import copy
import config
import gc
import math
from config import * #source of local  variables
import pickle
from multiprocessing import Pool
from functools import partial
import scipy as sp
import glob

class Host_Population(object):
    """
    This is a class containing the population of hosts.
    """
    def __init__(self):
        self.hosts = []
        self.total_hosts = host_pop_size-1
        self.treatment_prob = config.treatment_prob
        self.last_gen_infections = 0
       

    @staticmethod
    def found_population():
        """
        Produces new host population with randomly distributed ages.
        
        Parameters: None. 
        
        Usage: host_pop = genome_within_host.Host_Population.found_population()
        """
        new_pop = Host_Population()
        ages = rnd.randint(host_lifespan, size = host_pop_size)
        for i in range(host_pop_size):
            host = Host() 
            host.individual = i
            host.age = ages[i]
            new_pop.hosts.append(host) #each host is an object that is a property of the population
        return new_pop
        
    def initiate_infection(self):
        """
        Causes proportion of hosts to begin simulation infected with merozoites. All merozoite genomes are identical initially.
        
        Percentage to be infected is drawn from config. All hosts will be infected if perc_inf_host = 1 
        
        Parameters:
        arg1: host_population
        """
        n_infections = round(config.host_pop_size* config.perc_inf_host)
        if n_infections:
            infected_hosts = random.sample(self.hosts, n_infections) #list of hosts chosen from the population without replacement up to the number of hosts infected at start. 
            genome = Genotype()
            for individual in infected_hosts:
                for i in range(host_inoculation):
                    individual.mers.append(copy.deepcopy(genome))
                    individual.last_mers = host_inoculation
    
             
       
    def mosquito_feed(self):
        """
        Currently unused, can be adapted if bloodmeals come from multiple hosts
        """
        hosts_bitten = random.choices(list(range(1,len(self.hosts))), k = config.mosquitoes * rnd.poisson(config.number_bites)) #returns randomly
        start = 0
        for x in self.mosquitoes:
            self.mosquitoes[x].vector.bite(hosts_bitten[start,start+config.number_bites]) #steps thru list of random hosts, returning number_bites hosts to each mosquito to be bitten.
            start += config.number_bites
            
    def impose_treatment(self, time): 
        """
        Determines which hosts receive treatment from the population of symptomatic untreated infected hosts and maintains treatment for those currently being treated.
        
        Symptomatic hosts (sx_hosts) are the pool of hosts with merozoite counts high enough to make them eligible for treatment. 
    
        Treated hosts (tx_hosts) are randomly selected (without replacement) from symptomatic hosts up to the number of symptomatic hosts*probability that a symptomatic host receives treatment. If treatment_prob ==1, all symptomatic hosts are treated.
    
        As written, drug concentration is boolean. Can be modified if concentration varies due to drug half-life.
    
        If host is already being treated, treatment will continue until it has reached treatment duration. If a host's parasite load makes it eligible for treatment, it may immediately undergo a new round of treatment.
        
        
        Parameters:
        Arg1: host_population
        Arg2: time
        """
        if time > tx_introduction:
            treated_hosts = 0
            for host in self.hosts:
                if host.drug_conc:
                    host.treatment_duration+=1
                    if host.treatment_duration > treatment_length: #Ends treatment once duration of a hosts's treatment exceeds the standard treatment_length
                        host.treatment_duration = None
                        host.drug_conc = 0
                    else:
                        treated_hosts += 1 #If host is currently being treated, treatment duration is incremented by one 
            sx_hosts = [h for h in self.hosts if len(h.mers) > treatment_crit and h.drug_conc==0] #Untreated hosts with symptoms are selected       
            if sx_hosts:
                if self.treatment_prob == 1: #All symptomatic hosts are treated if treatment_prob == 1
                    for s in sx_hosts:
                        s.drug_conc = 1
                        s.treatment_duration = 1
                else:
                    if treated_hosts < round(len(sx_hosts)*self.treatment_prob): #if fewer hosts are being treated than treatment_prob*sx_hosts, more will begin treatment
                        new_tx = round(len(sx_hosts)*self.treatment_prob) - treated_hosts
                        tx_hosts = rnd.choice(sx_hosts, new_tx, replace=False) #Random symptomatic hosts will be chosen for treatment, number chosen determined by treatment_prob
                        for t in tx_hosts:
                            t.drug_conc = 1 
                            t.treatment_duration = 1

        
                
    @staticmethod
    def open_files(pid):
        """
        Opens data files.
    
        Parameters:
        Arg1: pid = operating system process ID. This ensures unique filenames.

        Usage: all_files = genome_within_host.Host_Population.open_files(pid)

        File names are formatted as pop_data_pid.txt, such that for pid = 6, pop_data_6.txt
    
        pop_data records for each host: Time, Host ID number, Number uninfected RBC, Number infected RBC commmitted to merozoites, Number merozoites, Number infected RBC commmitted to gametocytes, Number gametocytes, Number sporozoites, Drug concentration
    
        genome_data records data for each gametocyte in the population: Time, Host ID number, Genome fitness, # Resistance mutations in genome, # Tolerance mutations in genome, Drug concentration in host, Full genome
    
        vector_data records for each vector in vector_population: Time, Vector ID number, Infection (Boolean), Age, Number of pre-sporozoite genomes, Number of sporozoites

        strain_data records internal immune and exposure state for each host: Time, Host, Drug concentration, Parasitemia (current parasitemia of infection with all strains, Para_mort (host's individual parasitemia mortality threshold), plus Strain_RBC_x (count of RBCs of strain x) and Adaptive_x (adaptive immunity to strain x), for strains 1 to x
      
        epi_data records measures of endemicity: Time, Number of infected hosts, Number of exposed hosts, Number of symptomatic hosts, Number of contagious hosts, Number of infected vectors, Number of contagious vectors, % of infected hosts, % of exposed hosts, % of symptomatic hosts, % of contagious hosts, % of infected vectors, % of contagious vectors, Annual entomological exposure rate (calculated instantaneously from number of contagious vectors), R0 from last timestep

        Returns:
        all_files: a dict containing key:value pairs of filename:file {'pop_data':pop_data, 'genome_data':genome_data, 'vector_data':vector_data}
        """
        pop_data = open("pop_data" + str(pid) + ".txt", "a")
        pop_data.write("Time" + "\t" + "Host" + "\t" + "Age" + "\t" + "Host_uninfect_RBC" + "\t" + "Inf_Mer_RBC" + "\t" + "Mers" + "\t" + "Inf_Gam_RBC" + "\t" + "Gam" + "\t" + "Spor" + "\t" + "Drug_conc"  + "\t" + "Adaptive" + "\t" + "Innate" + "\t" + "preburst_mer_RBC" + "\t" + "Re"  + "\t" + "Tol1" + "\t" + "Tol2" + "\t" + "Res1" + "\t" + "Res2"+ "\t" + "Tol1Res1" + "\n")
        genome_data = open("genome_data" + str(pid) + ".txt", "a")
        gen_header = ["Time", "Host", "Fitness", "N_ResistMu", "N_TolMu", "Drug_conc", "Trans_invest", "Strain", "Sequence"]
        genome_data.write('\t'.join(map(str, gen_header)) + '\n')      
        vector_data = open("vector_data" + str(pid) + ".txt", "a")
        vector_data.write("Time" + "\t" + "Vector" + "\t" + "Infected" + "\t"+ "Age" + "\t" + "Genomes" + "\t" "Spor" + "\n")
        strain_data = open("strain_data" + str(pid) + ".txt", "a")
        strain_header = ["Time", "Host", "Drug_conc","Parasitemia", "Para_mort"]
        for i in range(n_strains):
            strain_header.append("Strain_RBC_"+str(i))
            strain_header.append("Adaptive_"+ str(i))
        strain_data.write("\t".join(map(str, strain_header)) + '\n')
        epi_data = open("epi_data" + str(pid) + ".txt", "a")
        epi_data.write("Time" + "\t" + "Inf_Host" + "\t" + "Exp_Host" + "\t" + "Sx_Host" + "\t" + "Cont_Host" + "\t" + "Inf_Vector"+ "\t" + "Cont_Vector" + "\t" + "Inf_Host_Perc" + "\t" + "Exp_Host_Perc" + "\t" + "Cont_Host_Perc" + "\t" + "Sx_Host_Perc" + "\t" + "Inf_Vector_Perc" + "\t" + "Cont_Vect_Perc" +"\t" + "AEIR" + "\t" + "R0" + "\n")
        file_keys = ['pop_data', 'genome_data', 'vector_data', 'strain_data', 'epi_data']
        file_vals = [pop_data, genome_data, vector_data, strain_data, epi_data]
        all_files = dict(zip(file_keys, file_vals))
        return all_files


    def write_data(self, all_files, vectpop, time):
        """
        Method called to write all data files.

        Parameters:
        Arg1: Host population
        Arg2: dict containing all files
        Arg3: Vector population
        Arg4: Time

        Usage:
        self.write_data(all_files, VectorPop, Time)

        """
        vect_infect, vect_cont = vectpop.write_vector_data(time, all_files['vector_data'])
        host_infect, host_exp, host_sx, host_cont = self.write_pop_data(time, all_files['pop_data'])
        self.write_epi_data(time, all_files['epi_data'], vect_infect, vect_cont, host_infect, host_exp, host_sx, host_cont)
        self.write_strain_data(time, all_files['strain_data'])
        if time%genome_data_write_freq == 0 or time == duration-1: #Because this writes a large amount of data, it is not called every multiple of genome_data_write_freq timesteps or in the last timestep
            self.write_genome_data(time, all_files['genome_data'])


    def write_epi_data(self, time, epi_data, vect_infect, vect_cont, host_infect, host_exp, host_sx, host_cont):
        """
        Method called by write_data() to calculate and write epi_data information
        Arg1: Host population
        Arg2: Time
        Arg3: epi_data file from all_files dict
        Remaining incoming arguments are those returned by write_pop_data() inside the write_data() function

        Modifies self.last_gen_infections to calculate R0 in the next time step

        """
        if vect_pop_size: # Only calculates vector properties and epidemic metrics if the vector population size > 0
            if self.last_gen_infections:
                r0 = host_infect/self.last_gen_infections
            else:
                r0 = host_infect
            epidata = [time, host_infect, host_exp, host_sx, host_cont, vect_infect, vect_cont, host_infect/host_pop_size, host_exp/host_pop_size, host_cont/host_pop_size, host_sx/host_pop_size, vect_infect/vect_pop_size, vect_cont/vect_pop_size, (vect_cont*365/uninf_feed_freq)/host_pop_size, r0]   
        else:
            epidata = [time, host_infect, host_exp, host_sx, host_cont, 0, 0, host_infect/host_pop_size, host_exp/host_pop_size, host_cont/host_pop_size, 0, 0, 0, 0]
        epi_data.write('\t'.join(map(str,epidata))+ '\n')  
        self.last_gen_infections = host_infect
            
    def write_pop_data(self, time, pop_data):
        """
        Method called by write_data() to calculate and write epi_data information
        Arg1: Host population
        Arg2: Time
        Arg3: pop_data file from all_files dict

        Returns:
        host_infect = Number of hosts that are infected (hosts for which host.is_infected() == T)
        host_exp = Number of hosts with past or current exposure
        host_sx = Number of hosts with merozoite abundance exceeding symptomatic threshold
        host_cont = Number of infected hosts with circulating mature gametocytes

        """
        host_infect = 0
        host_exp = 0
        host_cont = 0         
        host_sx = 0  
        for h in range(len(self.hosts)): #Counts number of parasites meeting criteria for various levels of tolerance and resistance. These are properties of the individual parasites
            tol1 = 0
            tol2 = 0
            res1 = 0
            res2 = 0
            tol1res1 = 0
            host = self.hosts[h]
            host.is_infected() #Boolean
            if host.infected:
                host_infect += 1
                infected_mer_RBC = sum(len(total) for total in host.infected_mer_RBC.values())
                if len(host.mers):
                    for m in host.mers:
                        if m.resist_mu_n == 1:
                            if m.tol_mu_n == 1:
                                tol1res1 += 1
                            else:
                                res1 += 1
                        elif m.resist_mu_n == 2:
                            res2 += 1
                        elif m.tol_mu_n == 1:
                            tol1 += 1
                        elif m.tol_mu_n == 2:
                            tol2 += 1
                    if len(host.mers) > treatment_crit:
                        host_sx += 1
                if len(host.gams):
                    host_cont += 1
            if sum(host.str_exposure.values()) or len(host.mers): #If host has logged any time exposed to any strain or currently has merozoites (representing first day after burst from liver), the host is considered exposed. Note this does not count otherwise-naive hosts with liver-stage infections
                host_exp += 1
            popdata = [time, host.individual, host.age, host.uninfected_RBC, sum(len(total) for total in host.infected_mer_RBC.values()), len(host.mers), sum(len(total) for total in host.infected_gam_RBC.values()), len(host.gams), sum(len(total) for total in host.sporozoites.values()), host.drug_conc, host.str_immunities[0], host.innate, host.preburst_mer_RBC, host.calc_mean_Re(), tol1, tol2, res1, res2, tol1res1]
            pop_data.write('\t'.join(map(str,popdata))+ '\n')
        return host_infect, host_exp, host_sx, host_cont


   
            
    def write_strain_data(self, time, strain_data):
        """
        Method called by write_data() to calculate and write strain_data information
        Arg1: Host population
        Arg2: Time
        Arg3: strain_data file from all_files dict
        
        Calculates number of infected RBCs to get parasitemia
        Writes RBC count and adaptive immunity for eash strain

        """
        for host in self.hosts:
            straindata = [time, host.individual, host.drug_conc, sum(host.RBC_strain_count.values())/host.uninfected_RBC, host.parasitemia_mort]
            for i in range(n_strains):
                if i in host.RBC_strain_count:
                    straindata.append(host.RBC_strain_count[i])
                else:
                    straindata.append(0)
                straindata.append(host.str_immunities[i])
            strain_data.write('\t'.join(map(str, straindata)) + '\n')                     


    def write_genome_data(self, time, genome_data):
        """
        Method called by write_data() to calculate and write genome_data information
        Arg1: Host population
        Arg2: Time
        Arg3: genome_data file from all_files dict
        
        Records individual properties of each gametocyte in the population

        """
        for host in self.hosts:
            if host.infected:
                if time in host.infected_gam_RBC:
                    for rbc in host.infected_gam_RBC[time]:
                        seq = ''
                        for n in rbc.genome:
                            seq += n
                        gendata = [time, host.individual, rbc.fitness, rbc.resist_mu_n, rbc.tol_mu_n, host.drug_conc, rbc.transmission_investment, rbc.strain, seq]
                        genome_data.write('\t'.join(map(str,gendata))+ '\n')
            else:
                next        

                   
    @staticmethod
    def close_files(all_files):
        """
        Closes all files
    
        Arg1: dict of all files returned by open_files()
    
        Usage: genome_within_host.Host_Population.close_files(all_files)
        """
        for i in all_files:
            all_files[i].close()

    def get_last_pickle(poptype):
        pickles=glob.glob(poptype+'*')
        ptimes = []
        for i in pickles:
            ptimes.append(int(i.split('_')[2]))
        ptimes.sort()
        pickname =poptype+'_Time_'+str(ptimes[-1])
        pickpop=glob.glob(pickname+'*')[0]
        return pickpop, ptimes[-1]


    def pickle_host_pop(self, time, pid):
        """
        Saves host population in a pickle
        Arg1: Host population
        Arg2: Time
        Arg3: Operating system process ID

        Usage:
        HostPop.pickle_host_pop(time, ProcessID)
        """
        with open("Hostpop_Time_" + str(time) + "_" + str(pid) + ".pickle", 'wb') as f:
            pickle.dump(self, f, pickle.HIGHEST_PROTOCOL)
        f.close()
    
    @staticmethod   
    def unpickle_host_pop(filename):
        """
        Restores saved host population object
        Arg1: Filename of pickle

        Returns: Host population

        Usage: 
        HostPop = genome_within_host.Host_Population.unpickle_host_pop("filename")

        """
        with open(filename, 'rb') as f:
            pop = pickle.load(f)
        return pop

    def host_pop_cycle(self, time):
        """
        Top level function to run all host population processes in parallel

        Parameters:
        Arg1: host population
        Arg2: time

        Usage:
        HostPop.host_pop_cycle(time) 
        """
        p = Pool()
        results = p.map_async(partial(Host.host_cycle, time = time), self.hosts) #All processes for individual hosts are contained within host_cycle()
        p.close()
        p.join()
        self.hosts = []        
        for a in results.get():
            if a.individual is None: #If a host has been replaced with a new host (indicated by a None individual ID, increment the number of hosts so the new host can be assigned a novel ID
                self.total_hosts+=1
                a.individual = self.total_hosts
            self.hosts.append(a)
        self.impose_treatment(time)


        
    def serial_host_cycle(self, time):
        """
        Top level function to run all host population processes in serial
        
        Parameters:
        Arg1: host population
        Arg2: time

        Usage:
        HostPop.serial_host_pop_cycle(time) 
        """
        mort_probs= rnd.uniform(0,1,len(self.hosts)) #Determine random mortalities, given background mortality rate
        for h in range(len(self.hosts)): #Go through infection cycle for each host
            self.hosts[h].infect_RBC(time)
            self.hosts[h].impose_mortality(time)
            self.hosts[h].burst(time)
            self.hosts[h].RBC_lifecycle(time)
            if self.hosts[h].serial_host_mortality(mort_probs[h]): #Check if host survives anemia, parasitemia, and age-based background mortality
                self.total_hosts += 1
                self.hosts[h] = Host()
                self.hosts[h].individual = self.total_hosts
                self.hosts[h].age = 0
            else:
                self.hosts[h].age += 1
        self.impose_treatment(time)

    


class Host(object):
    """
    Host class represents each individual within the Host_Population class. Hosts behave independently.
    
    Hosts are uninfected and unexposed at start of simulation. 
    
    Mortality occurs if a hosts's number of uninfected RBCs drops below a threshhold number. Dead hosts are replaced and host population size in constant. 
    
    Attibutes: 
    individual: Unique host ID #
    age: Host age in timesteps
    age_mort_rate: Not in use, included for backwards compatibility
    uninfected_RBC: Begins == to carrying_capacity_RBC, is increased to <= K with new RBC production in each timestep, is decreased by background RBC mortality and infection
    infected_gam_RBC: Dict of infected RBCs committed to gametocyte production with key:value pairs Time:[genomes]. Time is relevant if there is a burst delay
    infected_mer_RBC: Dict of infected RBCs committed to merozoite production with key:value pairs Time:[genomes]. Time is relevant if there is a burst delay
    mers: List of genomes that are in merozoite form
    gams: List of genomes that are in gametocyte form   
    sporozoites: Dict of sporozoite in liver phase with key:value pairs Time:[genomes]
    drug_conc: Drug concentration. Binary, 1 if currently being treated, 0 if not
    treatment_duration: How long host has been treated. None value if is not being treated.
    preburst_mer_RBC: Because infected merozite RBCs burst before data can be written, this attribute allows tracking how many there were
    last_mers: Merozoite count at last time step, for use in calculating Re based on current mer production
    infected: T/F for presence of merozoites, gametocytes, and infected RBCs. Sporozoites are not considered.
    innate: Level of innate immunity, starts at 0
    adaptive_growth: Individual host's adaptive growth rate
    parasitemia_mort: Individual host's hyperparasitemia mortality threshold
    str_immunities: Dict containing the adaptive immunity that has been generated against each strain. Does not include cross reactivity. Starts at 0.001 for each strain
    str_exposure: Dict containing the lifetime number of timesteps of exposure to each strain. Determines the extent of antigenic escape. Begins at 0 and does not reset after an infection ends
    RBC_strain_count: Dict containing number of infected RBCs for each strain
    total_antigen: Sum of circulating parasites: infected RBCs, gametocytes and merozoites

    """    

    __slots__ = ("individual","age","age_mort_rate","uninfected_RBC","infected_gam_RBC","infected_mer_RBC","mers","gams", "sporozoites","drug_conc", "treatment_duration", "preburst_mer_RBC", "last_mers", "infected", "innate", "adaptive_growth","parasitemia_mort", "str_immunities", "str_exposure", "RBC_strain_count", "total_antigen")

    
    def __init__(self):
        self.individual = None
        self.age = 0
        self.age_mort_rate = 0
        self.uninfected_RBC = carrying_capacity_RBC 
        self.infected_gam_RBC = {}
        self.infected_mer_RBC = {}
        self.mers = [] 
        self.gams = []    
        self.sporozoites = {}
        self.drug_conc = 0 
        self.treatment_duration = None
        self.preburst_mer_RBC = 0 
        self.last_mers = None
        self.infected = False
        self.innate = 0
        self.adaptive_growth = rnd.poisson(config.adaptive_growth*100)/100
        self.parasitemia_mort = rnd.poisson(config.parasitemia_mort*10)/1000 #multiplied by 10 for wider variation in poisson, divide by 1000 for percentage
        self.str_immunities = dict(zip(strains, [0.001]*n_strains))  
        self.str_exposure = dict(zip(strains, [0]*n_strains))  
        self.RBC_strain_count = {}
        self.total_antigen = 0   

        
    def __del__(self):
        del self
 
 
    def host_cycle(self, time):
        """
        Contains all individual host processes 

        Called by host_pop_cycle 

        Arg1: Host
        Arg2: time

        Returns: either self or a new host
        """
        sp.random.seed() #Prevents duplication in parallel
        self.infect_RBC(time) #Parasites infect host RBCs
        self.impose_mortality(time) #Mortality imposed on parasites. Includes host immune calculations
        self.burst(time) #Infected RBCs burst
        self.RBC_lifecycle(time)
        replaced = self.host_mortality() #If a host dies, a new one is created and returned
        if replaced:
            return replaced
        else:
            self.age += 1
            return self
                
    def impose_mortality(self, time): 
        """
        Manages mortality for parasites and infected RBCs.
        
        All merozoites die in each time step, infected RBCs die at constant background infected mortality rate+mortality from drug treatment+immune killing, gametocytes die at constant gam_mort_rate+immune killing
        
        For each time comtained in the keys of the infected_mer/gam_RBC dicts, the associated genomes are passed to infected_RBC_mort(). RBC mortality is calculated discretely in every generation instead of integrated because treatment status may change.
        
        Called by host_cycle()

        Parameters:
        Arg1: host
        """
        self.get_RBC_strain_count()
        self.check_innate_immunity()
        self.check_adaptive_immunity()
        self.is_infected()
        if self.infected: 
            if len(self.mers):
                self.last_mers = len(self.mers)
                self.mers.clear() #Any merozoites remaining from last time step die
            else:
                self.last_mers = None
            if self.gams:
                self.gams = self.parasite_mort(self.gams, gam_mort_rate) 
            self.infected_RBC_mort()            
        
    def is_infected(self):
        """
        Checks to see if a host is currently infected
        Counts number of gametocytes, merozoites, and infected RBCs to determine if host has active infection. 
        Modifies host.infected attribute to be True or False.

        Called by impose_mortality()
        
        Parameters:
        Arg1: host
        """
        self.total_antigen = (sum(len(total) for total in self.infected_mer_RBC.values())) + len(self.mers) + sum(len(total) for total in self.infected_gam_RBC.values()) + len(self.gams) 
        if self.total_antigen:
            self.infected = True
        else:
            self.infected = False
            
        
    def RBC_lifecycle(self, time): 
        """
        Increases the count of uninfected current RBCs by up to RBC_production (3.7e5 by default), not to exceed carrying_capacity_RBC (8.5e6 by default) 
        In an uninfected host, removes RBCs due to background mortality
        In an infected host, removed uninfected RBCs via bystander killing, a multiplier of the number of infected RBCs
        Bursts infected RBCs that have matured

        Called by host_cycle() and Host_Population.serial_host_cycle()
    
        Parameters:
        Arg1: host
        Arg2: time
        """
        if self.infected:
            inf_RBC = 0
            if time in self.infected_gam_RBC:   #Burst mature infected gametocyte RBCs
                inf_RBC += len(self.infected_gam_RBC[time])
            if mer_burst_delay:
                if time in self.infected_mer_RBC:   #Burst mature infected merozoite RBCs
                    inf_RBC += len(self.infected_mer_RBC[time]) 
            if not mer_burst_delay: #If there is no burst delay, all infected mer RBCs from this timestep will have burst before this step, so must use the count set pre-burst to calculate bystander killing
                inf_RBC += self.preburst_mer_RBC
            RBC_morts = round(inf_RBC*inf_mort_increase)
        else:
            RBC_morts = round(self.uninfected_RBC * RBC_mort_rate)
        self.uninfected_RBC -= RBC_morts
        if self.uninfected_RBC <= config.min_RBC_number: #If RBC count post-mortality is below the minimum before RBCs are replenished, host dies of anemia 
            self.uninfected_RBC = 0
        else:
            self.uninfected_RBC = min(self.uninfected_RBC + RBC_production, carrying_capacity_RBC) #new RBC will not exceed K

    def serial_host_mortality(self, prob):
        """
        Called from Host_Population.serial_host_cycle()
        Checks for each of 3 mortality conditions for a host: insufficient number of uninfected RBC, parasitemia higher than the host's threshold, or host age exceeding the lifespan parameter.
        
        Parameters:
        Arg1: host
        
        Returns:
        If any mortality condition is met: True
        If no mortality conditions are met: False
        """
        if (self.uninfected_RBC < config.min_RBC_number) or (sum(self.RBC_strain_count.values())/self.uninfected_RBC > self.parasitemia_mort): #death of host from disease by anemia or hyperparasitemia
            return True
        elif age_mort == True and self.age >= host_lifespan: 
            return True
        elif background_mort_rate:
            if prob < background_mort_rate:
                return True
        else:
            return False

    def host_mortality(self):
        """
        Called from host_cycle()
        Checks for each of 3 mortality conditions for a host: insufficient number of uninfected RBC, parasitemia higher than the host's threshold, or host age exceeding the lifespan parameter.
        
        Parameters:
        Arg1: host
        
        Returns:
        If any mortality condition is met: new Host
        If no mortality conditions are met: Nothing
        """
        if (self.uninfected_RBC < config.min_RBC_number) or (sum(self.RBC_strain_count.values())/self.uninfected_RBC > self.parasitemia_mort): #death of host from disease by anemia or hyperparasitemia
            new = Host()
            return new
        elif age_mort == True and self.age >= host_lifespan:  #Death from age
            new = Host()
            return new
        elif background_mort_rate:
            mortprob = random.uniform(0,1)
            if mortprob < background_mort_rate: #Death from random background mortality
                new = Host()
                return new
    

    def infect_RBC(self, time):
        """
        Called from host_cycle()

        This infects a host's RBCs using genomes returned by determine_reproducing_genotypes(), if host has merozoites.
        
        Each genotype returned by determine_reproducing_genotypes() infects an RBC and commits to either gametocytes or merozoites depending on comparison between random uniform prob & that genotype's transmission investment. 
        
        Newly infected RBCs of either type are appended to the infected_gam_RBC or infected_mer_RBC dict with key:value time:[genotype list]
        

        Parameters:
        Arg1: host
        Arg2: time
        
        """  
        self.preburst_mer_RBC = 0    
        if not self.mers: #tests if host has mers
            next
        else:
            chosen_genotypes = self.determine_reproducing_genotypes()
            if len(chosen_genotypes):
                self.uninfected_RBC -= len(chosen_genotypes) #removes RBCs from count of uninfected once they are infected
                new = rnd.uniform(0,1,len(chosen_genotypes)) 
                self.infected_mer_RBC[time] = [] #initialize a placeholder empty list
                self.infected_gam_RBC[time] = []
                for i in range(len(chosen_genotypes)):
                    if chosen_genotypes[i].transmission_investment < new[i]: #Determines if a genotype becomes a merozoite or gametocyte
                        self.infected_mer_RBC[time].append(chosen_genotypes[i])
                    else: 
                        self.infected_gam_RBC[time].append(chosen_genotypes[i])         
                del chosen_genotypes 
                random.shuffle(self.infected_mer_RBC[time])
                random.shuffle(self.infected_gam_RBC[time])
            else:
                next
    
    def determine_reproducing_genotypes(self): 
        """
        Called by infect_RBC()

        Calculates number of infections that will occur and chooses the genotypes of merozoites that will cause the infection with probability proportionate to fitness.

        if sum_fit = sum(fitlist) is used:
            (Default) A population with low fitness genotypes will produce fewer total infections than a population of same size with wild type genotypes. This assumes that cost of resistance causes an intrinsic growth rate decrease     
        
        if sum_fit = len(fitlist) is used:
            A population with low fitness gebotypes will produce the same number of infections as one with wild type genotypes but they will be disadvantaged in mixed infections (ie wild type genotypes will cause infections disproportionately). This assumes that cost of resistance only impairs competitive ability
        
        Parameters:
        Arg1: Host
        
        Returns:
        If infections occur and do not exceed the number of uninfected RBCs: List of genotypes selected (with replacement) with probability proportionate to fitness
        Otherwise: An empty list
        """
        fitlist = [genome.fitness for genome in self.mers] #Produces list containing fitness of genome in the population
        sum_fit = sum(fitlist) #Number of new infections cannot exceed summed fitnesses
        #sum_fit = len(fitlist) #Number of new infections cannot exceed number of merozoites
        infections = int(min(rnd.poisson(self.uninfected_RBC * infection_prob * sum_fit), sum_fit)) #How many total infections occur. 
        if not infections: #only if some cells have become infected
            return []
        if infections > self.uninfected_RBC:
            self.uninfected_RBC = 0 #Host will die in next host_cycle()
            return []
        else:
            new_genotypes = random.choices(self.mers, fitlist, k = infections) #Choices are weighted by fitness
            return new_genotypes
 

    def infected_RBC_mort(self):
        """
        Called by impose_mortality()

        Kills infected RBCs due to random background mortality, immunity, or drug treatment

        Arg1: Host
        """
        for tr in self.infected_mer_RBC: #Loops over each time
            self.infected_mer_RBC[tr] = self.parasite_mort(self.infected_mer_RBC[tr], inf_RBC_mort_rate) #Parasites decreased by background mort and immune killing
            if self.drug_conc: #If host is treated, chooses mortality probabiloty for those cells before sending them to RBC_treatment_mortality()
                tx_surviving_cells = []
                rand_nums = rnd.uniform(0,1,len(self.infected_mer_RBC[tr])) 
                for RBC in range(len(rand_nums)): 
                    new = self.infected_mer_RBC[tr][RBC].RBC_treatment_mortality(rand_nums[RBC])
                    if new:
                        tx_surviving_cells.append(new)
                self.infected_mer_RBC[tr] = tx_surviving_cells
        for tg in self.infected_gam_RBC: #Loops over each time
            self.infected_gam_RBC[tg] = self.parasite_mort(self.infected_gam_RBC[tg], inf_RBC_mort_rate) #Parasites decreased by background mort and immune killing
            if self.drug_conc: #If host is treated, chooses mortality probabiloty for those cells before sending them to RBC_treatment_mortality()
                tx_surviving_cells = []
                rand_nums = rnd.uniform(0,1,len(self.infected_gam_RBC[tg])) 
                for RBC in range(len(rand_nums)): 
                    new = self.infected_gam_RBC[tg][RBC].RBC_treatment_mortality(rand_nums[RBC])
                    if new:
                        tx_surviving_cells.append(new)
                self.infected_gam_RBC[tg] = tx_surviving_cells


    def parasite_mort(self, cells, mort_rate):
        """
        Called by burst() and infected_RBC_mort()

        Arg1: Host
        Arg2: list of gametocytes or merozoites
        Arg3: Constant mort_rate for that type of cell

        Returns: Cells that are not killed by backrgound mortality or immunity
        """
        mort = round(mort_rate*len(cells)) 
        surv_cells = cells[:-mort or None]
        final_cells = self.immune_mort(surv_cells)
        return final_cells
         


    def immune_mort(self, cells):
        """
        Called by parasite_mort()

        Calculates total immune killing power for each strain, based on current state of innate and strain-specific adaptive immunity, plus cross-reactive adaptive immunity, minus saturating immunity

        Applies total immune killing power (for each strain) to randomly kill a proportion of the list of cells (of that strain) that are passed in
        
        Parameters:
        Arg1: Host
        Arg2: List of genotypes of type:
            Infected RBCS if genotypes come from infected_RBC_immune_mort()
            Merozoites or gametocytes if genotypes are passed from parasite_mort()
        
        Returns:
        The list of cells which was passed in, minus genotypes killed by the immunes system 

        """
        surviving_cells = []
        max_innate = min(self.innate, 1)
        if n_strains == 1: #Crossreactivity is not calculated for more than one strain
            max_adaptive = min(self.str_immunities[0], 1)
            max_immune = ((innate_kill_rate*max_innate)+(adaptive_kill_rate*max_adaptive)) #gives total percent immune mortality           
            sat_immune = max_immune*max(1-max_sat, 1-(self.total_antigen/(saturation_shape+self.total_antigen))) # Immune killing power decreased by saturation
            pos_immune = max(sat_immune,0)
            tot_immune = min(pos_immune, 1) 
            mort = round(tot_immune * len(cells))
            surviving_cells = cells[:-mort or None]
        else:
            strain_count = self.get_parasite_strain_count(cells)
            surviving_counts = {}
            sorted_str_immune = sorted(self.str_immunities.values(), reverse = True) #Sorts adaptive immunities for all strains. Cross-reactivity is calculated from the highest non-self immunity 
            for s in strain_count:
                if self.str_immunities[s] == sorted_str_immune[0]: #sorted_str_immune[0] is the highest immunity so cross-reactivity is based on sorted_str_immune[1]
                    crossreact_immune = min(max_crossreact_immune, crossreactivity*sorted_str_immune[1])
                else:
                    crossreact_immune = min(max_crossreact_immune, crossreactivity*sorted_str_immune[0])
                max_adaptive = min(self.str_immunities[s]+crossreact_immune, 1)                       
                max_immune = ((innate_kill_rate*max_innate)+(adaptive_kill_rate*max_adaptive)) #gives total percent immune mortality
                sat_immune = max_immune*max(1-max_sat, 1-(self.total_antigen/(saturation_shape+self.total_antigen))) # Immune killing power decreased by saturation
                pos_immune = max(sat_immune,0)
                tot_immune = min(pos_immune, 1)                  
                survivors = strain_count[s] - round(tot_immune*strain_count[s]) #Total number of each strain that survives is put into a list
                if survivors:
                    surviving_counts[s] = survivors
            while len(surviving_counts):
                for cell in cells: #Loops through all cells and sorts out survivors for each strain, stops when all survivors have been selected
                    if cell.strain in surviving_counts:
                        surviving_counts[cell.strain] -= 1
                        surviving_cells.append(cell)
                        if not surviving_counts[cell.strain]:
                            del surviving_counts[cell.strain]
        return surviving_cells




        
    def check_innate_immunity(self):
        """
        Called by impose-mortality()

        Calculate level of innate immunity based on current count of infected RBCs of all strains

        Innate immunity cannot become negative
        
        Parameters:
        Arg1: host
        """
        cap_innate = min(self.innate, 1)
        total_RBC = sum(self.RBC_strain_count.values())
        growth = min(innate_growth*total_RBC, 1)
        newZ = growth-(innate_decay*self.innate)
        self.innate = max(self.innate + newZ, 0) #innate immunity will not become negative
            
    def check_adaptive_immunity(self):
        """
        Called by impose_mortality()

        Calculate level of adaptive immunity for each strain. Growth function if strain RBCs are present, else decay function.
        
        Parameters:
        Arg1: Host
        """
        for strain in self.str_immunities: #For all strains host has been exposed to
            if strain in self.RBC_strain_count:
                newI = ((self.adaptive_growth*self.str_immunities[strain]*(self.RBC_strain_count[strain]/(adaptive_shape + self.RBC_strain_count[strain])))) - (antigenic_escape*self.str_immunities[strain]*(1-((self.str_exposure[strain]**decay_coefficient)/((self.str_exposure[strain]**decay_coefficient) + (antigenic_shape**decay_coefficient)))))
            else:
                newI = -adaptive_decay*self.str_immunities[strain]
            self.str_immunities[strain]= max(self.str_immunities[strain] + newI, 0.001) #adaptive immunity will not drop below baseline
            self.str_immunities[strain] = min(self.str_immunities[strain], 1) #fraction of activated immune cells cannot exceed 1


    def get_RBC_strain_count(self):
        """
        Called by impose_mortality()

        Counts infected RBCs for each strain
        Increments duration of strain exposure for every strain present

        Arg1: Host
        """
        self.RBC_strain_count = {} #Empty dict to reset count in every timestep
        if config.n_strains == 1:
            RBC_count = 0
            for mr in self.infected_mer_RBC:
                RBC_count += len(self.infected_mer_RBC[mr])
            for gr in self.infected_gam_RBC:
                RBC_count += len(self.infected_gam_RBC[gr])
            if RBC_count:
                self.RBC_strain_count = {0:RBC_count}
        else:
            for tr in self.infected_mer_RBC:
                for genotype in self.infected_mer_RBC[tr]:
                    if genotype.strain in self.RBC_strain_count: #If the strain has already been added to dict, increment the count
                        self.RBC_strain_count[genotype.strain] += 1
                    else:
                        self.RBC_strain_count[genotype.strain] = 1   #Otherwise, create a key for that strain             
            for tg in self.infected_gam_RBC:
                for genotype in self.infected_gam_RBC[tg]:
                    if genotype.strain in self.RBC_strain_count:
                        self.RBC_strain_count[genotype.strain] += 1
                    else:
                        self.RBC_strain_count[genotype.strain] = 1 
        for s in self.RBC_strain_count: #Increment exposure for every strain is present (which has been added to the dict)
            self.str_exposure[s] += 1


    def get_parasite_strain_count(self, cells): 
        """
        Called by immune_mort()

        Gets count for each strain from list of cells passed in

        Arg1: Host
        Arg2: List of parasites passed from immune_mort()

        Returns: dict containing counts for each strain
        """
        if config.n_strains == 1:
            par_strain_count = {0:len(cells)}
        else:
            par_strain_count = {}
            for genotype in cells:
                if genotype.strain in par_strain_count:
                    par_strain_count[genotype.strain] += 1
                else:
                    par_strain_count[genotype.strain] = 1  
        return par_strain_count    

            
    
    def burst(self, time): 
        """
        Called by host_cycle() and Host_Population.serial_host_cycle()

        Checks time of all infected cells (infected_mer_RBC, infected_gam_RBC, sporozoites) to determine if they have completed their maturation (ie if there are genotypes present at current_time - burst_delay). 
        
        If so, they burst and release a number of parasites which equals the burst size for that type of cell.
        
        New merozoites immediately experience immune and background mortality. Gametocytes do not experience it until impose_mortality() is called by host_cycle() 
        
        Parameters:
        Arg1: Host
        Arg2: Time
        """
        if time-gam_burst_delay in self.infected_gam_RBC: #If there are matured gametocyte RBCs
            for genome in self.infected_gam_RBC[time-gam_burst_delay]:
                self.gams.extend(genome.reproduce(1))           #an infected_gam_RBC is equivalent to an immature gametocyte. It is reproduced and added to the host's gametocytes
            del self.infected_gam_RBC[time-gam_burst_delay]
        random.shuffle(self.gams)
        newmers = []
        self.preburst_mer_RBC = sum(len(total) for total in self.infected_mer_RBC.values())
        if time-spor_delay in self.sporozoites: #If there are matured sporozoites
            for genome in self.sporozoites[time-spor_delay]:
                newmers.extend(genome.reproduce(spor_burst_size))  #Sporozoites produce merozoites
            del self.sporozoites[time-spor_delay]                   
        if time-mer_burst_delay in self.infected_mer_RBC: #If there are matured merozoite RBCs
            for genome in self.infected_mer_RBC[time-mer_burst_delay]:
                newmers.extend(genome.reproduce(genome.mer_burst_size)) 
            del self.infected_mer_RBC[time-mer_burst_delay]            
        random.shuffle(newmers)
        mer_morts = rnd.poisson(mer_mort_percent)*0.01 #Background mortality
        self.mers = self.parasite_mort(newmers, mer_morts) #Merozoites are killed by immunity & drugs


    def calc_mean_Re(self):
        """
        Called by write_pop_data()

        Compares current merozoite count at time t to count at time t-1 to measure growth of infection in host

        Returns:
        If host is infected:
            if host was infected in the last timestep: #merozoites at time t/#merozoites t-1
            if host was not infected in the last timestep: 0
        If host is not infected: "NA"
        """
        if self.last_mers:
            if self.mers:
                return len(self.mers)/self.last_mers
            else:
                return 0
        else:
            return "NA"
        

class Genotype(object):
    """
    Genotype class represents the genome of each parasite in the vector and host populations.
    
    Any given genotype objects is categorized as one of the following: a merozoite, a gametocytes, an oocyst, an infected mer RBCs, an infected gam RBCs, or a sporozoites. This is designated by what they are an attribute of. 
    IE a given Genotype object can begin as a merozoite, be reassigned to become an infected_gam_RBC after infecting an RBC, burst into a gametocyte, enter a vector, become an oocyst, and then mature into a sporozoite to be transmitted to a host by changing where it is indexed rather than by changing any properties of the genotype.
    
    Each genotype experiences recurrent mutation at each locus. Fitness is determined by the values at resistance_loci, such that decreased fitness of a resistance locus also provides a level of resistance to drug therapy.
    
    Fitness only affects the probability of reproduction rather than mortality rate or the probability of avoiding immune attack.

    
    Attibutes: 
    genome: List of values of length n_loci. Start with a value of "A" and experience probability of mutation in every generation. 
    fitness: Fitness is 1 for wild type, and is less than 1 if a resistance mutation is present and resistance is costly
    mer_burst_size: Number of merozoites produced when an infected_mer_RBC bursts
    transmission_investment: transmission investment 0-1, where 1 means only gametocytes will be produced and 0 means only merozoites will be produced
    resist_mu_n: number of mutants which match the resistance value
    tol_mu_n: number of mutants which match the tolerance value
    mu_trans_investment: (T/F) allow transmission investment to mutate
    strain: Strain ID
    high_rec: (T/F) If TRUE, recombination will always occur between the two resistance/fitness loci. Excludes genome_rec.
    genome_rec: (T/F) Free recombination between the two resistance/fitness loci. Excludes high_rec.
    strain_rec: (T/F) Free recombination between strain locus and resistance/fitness loci
    
    Oocyst burst size is a constant, not property of genome. 
    """
    __slots__ = ("genome", "fitness", "gam_burst_size","mer_burst_size","transmission_investment", "resist_mu_n","tol_mu_n","mu_trans_investment", "strain", "high_rec", "genome_rec", "strain_rec")
    
    def __init__(self):
        self.genome = [founding_nt] * n_loci
        self.fitness = 1
        self.gam_burst_size = config.gam_burst_size
        self.mer_burst_size = mer_burst_size
        self.transmission_investment = transmission_investment
        self.resist_mu_n = 0
        self.tol_mu_n = 0 
        self.mu_trans_investment = mu_trans_investment 
        self.strain = 0
        self.high_rec = high_rec
        self.genome_rec = genome_rec
        self.strain_rec = strain_rec

         
    def reproduce(self, burstsize):
        """
        Called by Host.burst() and vector.Vector.produce_sporozoites()

        Mutates a parent genotype, calculates its fitness, and produces exactly burstsize offspring.
        
        Because only the parent is mutated, all offspring will be clones of one another.
        
        Parameters:
        Arg1: Genotype (bursting RBC, sporozoite, or oocyst)
        Arg2: Relevant burst size for a parasite in that stage (e.g. mer_burst_size)
        
        Returns:
        List of new genotypes of length burstsize.
        """
        offspring = []
        self.mutate_genome()   
        self.calc_fitness()
        for i in range(burstsize):
            new = copy.deepcopy(self)
            offspring.append(new)
        return offspring
        
    def mutate_genome(self):
        """
        Called by reproduce()

        Mutates resistance/fitness loci, mutates transmission investment if True, mutates strain if number of strains > 1
        
        The genotype's transmission investment is treated as an individual locus, but the value of the individual mutations is constrained to a value given as max_TI_mu which defaults to 5%.
        
        Parameters:
        Arg1: Genotype
        """
        n_mutations = min(rnd.poisson(mutation_rate*len(self.genome)), len(self.genome)) # n_mutations cannot exceed length of genome 
        mut_loci = random.sample(range(len(self.genome)), n_mutations) # loci chosen to be mutated
        for i in range(n_mutations):
            mut_nt = self.genome[mut_loci[i]]
            while self.genome[mut_loci[i]] == mut_nt: #Mutates to a non-self nucleotide
                mut_nt = random.choice(nts)
            self.genome[mut_loci[i]] = mut_nt
        if self.mu_trans_investment: #Mutate transmission investment
            n_ti_mu = rnd.poisson(ti_mutation_rate)
            if n_ti_mu:
                TIval = rnd.normal(0, max_TI_mu) 
                minTI = max(self.transmission_investment+TIval, 0) #if current TI plus generated T is negative, will return 0
                self.transmission_investment = min(minTI, 1) #if current TI plus generated T exceeds 1, will return 1
        if n_strains > 1:
            self.mutate_strain() 

    def mutate_strain(self):
        """
        Called by mutate_genome()

        Mutates the identity locus (strain identifier) of a genotype to a different identity with probability strain_mu_prob. 
        If it mutates, the new strain ID locus will be selected from the list in strain_vals.
        
        Parameters:
        Arg1: Genotype
        """
        p_mutate = random.random()
        if p_mutate < strain_mu_prob:
            new_strain = self.strain
            while self.strain == new_strain: # Chooses a non-self strain
                new_strain = random.choice(strains)
            self.strain = new_strain
    
          
    def calc_fitness(self):
        """
        Called by reproduce()

        Calculates fitness of a genome based on the values at the resistance loci. Fitness only affects probability of infecting RBCs and probability that infected RBCs survive drug treatment.
        
        Fitness is multiplicative between loci. If there are resistance or tolerance mutations, they reduce fitness. Otherwise, fitness is 1. 

        If having the maximum number of resistance mutations should produce fitness with a different value, that is set with compensated_res_s. If 2 mutations causes no additional fitness cost, compensated_res_s == resist_mu_s
        
        Tolerance and resistance threshold are used if tolerance/resistance are determined from multiple nucleotides within a locus, ie in a 3-nucleotide resistance locus, any 2 values mutated may produce tolerance, and all 3 may produce resistance. 
        Here, no tolerance is used. Each locus is 1 nt, and 1 is the resistance threshold
        
        If either tolerance or resistance is not used, the threshold can be a set to a number greater than the number of bases in the resistance locus.


        Parameters:
        Arg1: Genotype
        """
        rmus = []
        self.tol_mu_n = 0
        self.resist_mu_n = 0
        tempfit = 1
        for gene in range(len(resistance_loci)): #Check how many sites within a resistance gene have a resistance mutation value
            mu = 0
            for site in range(len(resistance_loci[gene])):
                if self.genome[resistance_loci[gene][site]] == res_vals[gene][site]:
                    mu+= 1
            rmus.append(mu) #List of total number of resistance mutations in each resistance gene
        for m in rmus:
            if m == tol_threshold: 
                self.tol_mu_n += 1
                tempfit *= tol_mu_s # multiplicative epistasis decreases the fitness for each tolerance mutation
            if m == resist_threshold: # multiplicative epistasis decreases the fitness for each resistance mutation
                self.resist_mu_n += 1
                tempfit *= resist_mu_s
            if self.resist_mu_n == len(resistance_loci): # however, if all possible resistance sites have resistance mutations, fitness is set to compensated_res_s
                tempfit = compensated_res_s
        self.fitness = tempfit


        
    def recombine(self, other):
        """
        Called by vector.Vector.produce_oocysts()

        Recombines two gametocyte genotypes and produces a single offspring. Transmission investment is treated as unlinked locus.
        
        Parameters:
        Arg1: Genotype 1
        Arg2: Genotype 2
        
        Returns: Genotype 
        """
        offspring = copy.deepcopy(self) #Copied instead of creating a new genotype() because will use genome and all relevant attributes will be recalculated. This will only be an issue as written if different genotypes have different burst sizes 
        if genome_rec:
            if high_rec and len(self.genome) == 2: #guaranteed recombination for biallelic genome
                offspring.genome[1] = other.genome[1]
            else:
                n_recs = rnd.poisson(rec_num) #Number of crossover events
                if n_recs:
                    rec_sites= [0, len(offspring.genome)] #Create a list with beginning and endpoints, where no recombinations can happen by definition
                    try:
                        rec_sites.extend(random.sample(list(range(1,len(offspring.genome))),n_recs)) #Chooses n_recs random sites for crossover
                        rec_sites.sort() #Puts rec_sites in sequential order
                    except ValueError:
                        rec_sites = list(range(1,len(offspring.genome))) 
                    for segment in list(range(len(rec_sites))):
                        if segment%2 == 1 and rec_sites[segment] != len(offspring.genome): #Even number segments will correspond to self genome so don't need to do anything
                            seglength = list(range(rec_sites[segment], rec_sites[segment+1])) #Generates list of numbers corresponding to loci from range between two sequential rec sites
                            offspring.genome[rec_sites[segment]:rec_sites[segment+1]] = other.genome[rec_sites[segment]:rec_sites[segment+1]] #switches those loci to equal values from other genome
        if n_strains > 1 and strain_rec:
            offspring.strain = rnd.choice([self.strain, other.strain])
        if self.mu_trans_investment:
            offspring.transmission_investment = rnd.choice([self.transmission_investment, other.transmission_investment])                
        return offspring
   
                
    def RBC_treatment_mortality(self, rndnum): 
        """
        Called by Host.infected_RBC_mort()

        Calculate the probability that an individual infected RBC will die when there is drug treatment.

        Parameters:
        Arg1: Genotype
        Arg2: a random number drawn from a uniform distribution
        
        Returns:
        If genotype survives: self genotype
        Otherwise: None
    
        """
        if self.resist_mu_n == len(resistance_loci) : #if all possible resistance sites have resistance mutations, the individual is totally drug resistant 
            resistance_value = 0
        else:
            resistance_value = treatment_efficacy_tol**self.tol_mu_n * treatment_efficacy_res**self.resist_mu_n #Otherwise, resistance is multiplicative
        mort_prob = resistance_value*drug_efficacy 
        if mort_prob > rndnum:
           return None
        else:
            return self
    
        

          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
            
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
            
    
            
