import os
import genome_within_host
from config import *
import vector
import time

start = time.time()
pid = os.getpid()

last_hostpop, d = genome_within_host.Host_Population.get_last_pickle('Hostpop') #Find pickles with the latest timestep
last_vectpop, d = genome_within_host.Host_Population.get_last_pickle('Vectpop') # Returns last timestep as d
print(last_vectpop, d)
hostpop = genome_within_host.Host_Population.unpickle_host_pop(last_hostpop)
vectpop = vector.Vector_Population.unpickle_vect_pop(last_vectpop)

hostpop.treatment_prob = treatment_prob
all_files = genome_within_host.Host_Population.open_files(pid)

now=time.time()


while now-start < 85800 and d <100000:
    d+=1
    hostpop.host_pop_cycle(d) 
    vectpop.vector_cycle(d, hostpop)
    print(d)
    if d%data_write_freq == 0: 
        hostpop.write_data(all_files, vectpop, d)
    if d%200 == 0:
        for eafile in all_files:
            all_files[eafile].flush()
    now = time.time()
    
if d%data_write_freq:
    hostpop.write_data(all_files, vectpop, d)
if d%genome_data_write_freq:
    hostpop.write_genome_data(d, all_files['genome_data'])  
        
        
genome_within_host.Host_Population.close_files(all_files)
hostpop.pickle_host_pop(d, pid)
vectpop.pickle_vect_pop(d, pid)

