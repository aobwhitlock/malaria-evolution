import genome_within_host
import numpy as np
import copy
import random
import numpy.random as rnd    
from config import *    
import math
import config
import pickle
from multiprocessing import Pool
from functools import partial
import scipy as sp

class Vector_Population(object):
    """
    This class contains the population of vectors
    """
    def __init__(self):
        self.vectors = []
        self.vect_pop_size = config.vect_pop_size
        self.infected_vectors = 0
        self.total_vectors = vect_pop_size-1
    
    @staticmethod   
    def found_vect_pop():
        """
        Parameters: None. Static method of Vector_Population() class. 
    
        Usage: VectorPopulation = vector.Vector_Population.found_vect_pop()

        Returns: Population of vectors
        """
        vect_pop = Vector_Population()
        ages = rnd.uniform(0, vect_lifespan, vect_pop_size)
        for i in range(vect_pop_size):
            new = Vector()
            new.individual = i
            new.age = math.floor(ages[i])          
            if new.age%3 == 0: #Vectors feed every time their age is divisible by 3
                new.next_feed == 0
            else:
                new.next_feed = uninf_feed_freq - new.age%uninf_feed_freq #vector will feed at the soonest age that is dividible by the uninf_feed_delay so feeding is asynchronous
            vect_pop.vectors.append(new)            
        return vect_pop
        
        
    def vector_cycle(self, time, host_pop):
        """
        High level function called from top level script, containing all activities undergone by the vector population.
        
        Parameters:
        Arg1: VectorPopulation (self)
        Arg2: Time
        Arg3: HostPopulation

        Usage: VectorPopulation.vector_cycle(Time, HostPopulation)

        """
        self.pop_feed(time, host_pop)
        self.vector_lifecycle(time)
        for vect in self.vectors:
            if time-infectious_delay in vect.genomes: #If vectors have been infected long enough, the genomes they contain become sporozoites which will be transmitted at the next feeding
                vect.produce_sporozoites(time)          


    def pop_feed(self, time, host_pop): 
        """
        Called by vector_cycle()

        Selects all vectors that are due to feed at this time and pairs them with random hosts chosen (with replacement) from the host population for feeding.
        
        Parameters: 
        Arg1: Vector population (self) 
        Arg2: Time
        Arg3: HostPopulation
        """
        feeding_vects = [f for f in self.vectors if f.next_feed == time]
        bitten_hosts = rnd.choice(host_pop.hosts, len(feeding_vects)) #hosts can be bitten more than once
        for v in list(range(len(feeding_vects))):
            feeding_vects[v].feed(bitten_hosts[v], time) #Each feeding vector is paired with one host
      
                       
    def vector_lifecycle(self, time): 
        """   
        Called by vector_cycle()

        Determines survival of each individual in vector_population based on age and mortality rate. Vectors that die are replaced to maintain steady population size 
        
        Parameters: 
        Arg1: VectorPopulation (self)
        Arg2: Time
        
        Returns: Modifies vector_population.vectors in place
        """
        livingvectors = [self.vectors[v] for v in list(range(len(self.vectors))) if self.vectors[v].age < vect_lifespan] # Vectors survive if their age is less than lifespan
        for l in livingvectors: # Increment survivors' ages
            l.age+=1
        newvectors = [Vector() for x in list(range(len(livingvectors),self.vect_pop_size))]  #produces individuals to replace those that died up to Vector pop size
        for n in newvectors:
            self.total_vectors += 1
            n.individual = self.total_vectors
            n.next_feed = time+1 # All new vectors will feed in the next timestep
        livingvectors.extend(newvectors)
        self.vectors = livingvectors    

    def write_vector_data(self, time, vector_data):
        """
        Called by genome_within_host.Host_Population.write_data()

        Arg1: Vector population
        Arg2: Time
        Arg3: vector data file from all_files dict

        Returns:
        Number of infected vectors
        Number of contagious vectors (vectors containing mature sporozoites for transmission)
        """
        vect_infect = 0
        vect_cont = 0
        if self.vectors:
            for vec in self.vectors:
                vec.is_infected()
                if vec.infected:
                    vect_infect += 1
                if len(vec.sporozoites):
                    vect_cont += 1       
        return vect_infect, vect_cont  
        
    def pickle_vect_pop(self, time, pid):
        """
        Saves vector population as pickle

        Arg1: Vector population
        Arg2: Time
        Arg3: OS process ID

        Usage: VectorPopulation.pickle_vect_pop(Time, pid)

        Saves pickle in form Vectpop_Time_time_pid.pickle

        """
        with open("Vectpop_Time_" + str(time) + "_" + str(pid) + ".pickle", 'wb') as f:
            pickle.dump(self, f, pickle.HIGHEST_PROTOCOL)
        f.close()
    
    @staticmethod  
    def unpickle_vect_pop(filename):
        """
        Restores vector population from pickle
        
        Arg1: Filename of vector population

        Returns vector population
        """ 
        with open(filename, 'rb') as f:
            pop = pickle.load(f)
        return pop

        
class Vector(object):
    """
    Vector class represents each individual within the Vector_Population(). 
    
    Vectors are uninfected at start of simulation. When they become infected, they pick up gametocytes which recombine and experience mortality over the course of parasite development (stages not distinguished). After infectious_delay, sporozoites are produced and vectors are infectious.
    
    Vectors feed at birth. If they do not become infected,they feed every 3 timesteps. Infections introduce delay of length inf_feed_delay to the time of the vector's next feeding. 
    
    
    Attibutes: 
    individual: Unique ID for vector individual
    genomes: Dict where keys = time of infection and values = genomes ingested at that time point
    infected: Boolean. True if individual is currently infected.
    sporozoites: List of all sporozoites. An individual with sporozoites is infectious.
    age: Number of time steps since vector's birth
    uninf_feed_freq: How many time steps between feedings 
    next_feed: Individuals feed at birth. At each feeding, next_feed is incremented by uninf_feed_freq (+ inf_feed_delay if individual becomes infected while feeding)
    bloodmeal_size: Number of uL of blood ingested in a single bite. As implemented, entire bloodmeal is taken from one host. 
    """
    def __init__(self):   
        __slots__ = ("individual","genomes","infected","sporozoites","age","uninf_feed_freq", "next_feed", "bloodmeal_size")        
        self.individual = None
        self.genomes = {} 
        self.infected = False 
        self.sporozoites = []
        self.age = 0 
        self.uninf_feed_freq = config.uninf_feed_freq 
        self.next_feed = 0 
        self.bloodmeal_size = config.bloodmeal_size 


    def is_infected(self):
        """
        Called by Vector_Population.write_vector_data()

        Checks if vector has genomes or sporozoites

        Returns:
        Modifies vector.infected value to True or False
        """
        if len(self.sporozoites) + sum(len(total) for total in self.genomes.values()):
            self.infected = True
        else:
            self.infected = False
            
    def feed(self, host, time): 
        """
        Called by Vector_Population.pop_feed()

        A vector feeds on a host.
        
        If the vector has sporozoites, they will be transmitted to the host with 100% probability. There is no dose dependency. These sporozoites are removed from the vector. 
        
        If the host has gametocytes, the vector will become infected with a density-based probability function.
        
        If the vector becomes infected, the genomes it ingests will immediately undergo recombination and mortality representing stages of development.
        
        Parameters:
        Arg1: Vector
        Arg2: Host
        Arg3: Time
        
        Returns:
        Modifies host.sporozoites and vector.genomes in place.
        """
        self.next_feed += uninf_feed_freq #Increment to next feeding time
        if self.sporozoites:
            n_transmitted = min(rnd.poisson(inoculation_size), len(self.sporozoites)) #Random Pois number of sporozoites will be transmitted. 
            host.sporozoites[time] = self.sporozoites[len(host.sporozoites)-n_transmitted:] #n_transmitted genomes are removed from vector (no replacement) and added to host. Genomes taken from end of list for efficiency, but the burst process has already put them in random order.
            del self.sporozoites[len(host.sporozoites)-n_transmitted:]       
        if host.gams:
            if self.calc_vector_infection_probability(host, time): 
                self.infected = True
                self.next_feed += inf_feed_delay
                self.produce_oocysts(time)
                
                
    def calc_vector_infection_probability(self, host, time):
        """
        Called by feed()

        Calculates probability of infection given the abundance of gametocytes and infects the vector if appropriate.
        
        If an infection occurs, random gametocytes are added to the vector's genomes (with replacement, not removed from host because the number of parasites within a host >>> parasites in a bloodmeal) with a key of current time.
        
        Parameters:
        Arg1: Vector
        Arg2: Host
        Arg3: Time
        
        Returns:
        True/False, with True indicating transmission

        """
        pInfect = (alpha*(len(host.gams)**beta))/(1+gammaval*(len(host.gams)**beta))
        infect = rnd.uniform(0,1)
        if pInfect >= infect:
            mealgam = rnd.poisson(self.bloodmeal_size*len(host.gams)) 
            if mealgam:
                self.genomes[time] = copy.deepcopy(rnd.choice(host.gams, mealgam))
                return True
        else:
            return False       
            
    
    def produce_oocysts(self, time):
        """
        Called by feed()

        Gametocytes go through recombination, mortality, and all stages of development in a single step 
        
        After recombination and mortality, surviving offspring remain in genome list until time+infectious_delay, at which point they become sporozoites
        
        Parameters:
        Arg1: Vector
        Arg2: Time
        
        Modifies vector.genomes[time]
        """
        N_surviving_genomes = max(round(len(self.genomes[time]) * in_vect_survival_rate), 1) #N_surviving_genomes = how many offspring will survive to become oocysts
        if recombine:
            parents2 = random.sample(list(self.genomes[time]), N_surviving_genomes) #parents 2 are chosen from entire list of parents1 (without replacement), but only as many as will survive so no inconsequential recombination occurs
            self.genomes[time] = [self.genomes[time][x].recombine(parents2[x]) for x in range(len(parents2))] #Genomes at current time now consists  only of recombined offspring
        else:
            self.genomes[time] = self.genomes[time][0:N_surviving_genomes]


    def produce_sporozoites(self, time):
        """
        Called by vector_cycle()

        Genomes which have survived through the infectious_delay undergo random mutation and then burst into mature sporozoites.
        
        Exactly oocyst_burst_size sporozoites are produced per oocyst. Note: only oocyst is mutated so individual sporozoites from same oocyst have identical genomes.
        
        These offspring become/are added to the vector's list of sporozoites. They are then randomly shuffled so that they are equally likely to be transmitted to newly bitten hosts.
        
        Parameters:
        Arg1: Vector
        Arg2: Time - infectious_delay
        """
        for o in self.genomes[time-infectious_delay]:
            new = o.reproduce(oocyst_burst_size)
            if self.sporozoites:
                self.sporozoites.extend(new)
            else:
                self.sporozoites = new
        random.shuffle(self.sporozoites)
        del self.genomes[time-infectious_delay] #Burst oocysts are removed from list of genomes


